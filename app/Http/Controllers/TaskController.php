<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    public function index()
    {
        return Task::orderBy('created_at', 'asc')->get()->map(function($task)
        {
            $task->finished = (bool) $task->finished;
            return $task;
        });
    }

    public function addTask(Request $request)
    {
        return Task::create(['name' => $request->name, 'finished' => false]);
    }

    public function finishTask(Request $request)
    {
        return [
           'success' => Task::where('id', $request->id)->update(['finished' => true])
       ];
    }

    public function startTask(Request $request)
    {
        return [
           'success' => Task::where('id', $request->id)->update(['finished' => false])
        ];
    }
}
