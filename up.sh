composer install --no-dev;
php artisan key:generate;
php artisan migrate;
php artisan config:cache;
php artisan route:cache;
npm install --no-dev --no-bin-links;
npm run production;
