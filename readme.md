# Task Manager

## Server Requirements
The Laravel framework has a few system requirements. All of these requirements are satisfied by the Laravel Homestead virtual machine, so it's highly recommended that you use Homestead as your local Laravel development environment.

However, if you are not using Homestead, you will need to make sure your server meets the following requirements:

- PHP >= 7.2.0
- BCMath PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Node.js >= 12.13.0
- npm >=  6.12.0

## Installation
1. Create domain for this site.
2. Create **.env** file with configuration for mysql database and url for domain.
3. Run **up.sh** file
4. Open your domain.
